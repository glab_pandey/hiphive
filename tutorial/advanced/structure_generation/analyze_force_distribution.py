"""
Generate and plot distributions of displacements, distances, and forces.
Forces are calculated using the EMT calculator.
"""
import numpy as np
import matplotlib.pyplot as plt
from collections import OrderedDict
from ase.io import read
from ase.calculators.emt import EMT

bins = {'displacement': np.linspace(0.0, 0.7, 80),
        'distance': np.linspace(1.0, 4.5, 150),
        'force': np.linspace(0.0, 8.0, 50)}


def get_histogram_data(data, bins=100):
    counts, bins = np.histogram(data, bins=bins, density=True)
    bin_centers = [(bins[i+1]+bins[i])/2.0 for i in range(len(bins)-1)]
    return bin_centers, counts


def get_distributions(structure_list, ref_pos, calc):
    """ Get distributions of interatomic distances, and displacements.

    Parameters
    ----------
    structure_list : list of ASE Atoms objects
        list of structures used for computing distributions
    ref_pos : Numpy array (N, 3)
        reference positions used for computing the displacements
    calc : ASE Calculator object
        calculator used for computing forces
    """
    distances, displacements, forces = [], [], []
    for atoms in structure_list:
        distances.extend(atoms.get_all_distances(mic=True).flatten())
        displacements.extend(np.linalg.norm(atoms.positions-ref_pos, axis=1))
        atoms.set_calculator(calc)
        forces.extend(np.linalg.norm(atoms.get_forces(), axis=1))
    distributions = {}
    distributions['distance'] = get_histogram_data(distances, bins['distance'])
    distributions['displacement'] = get_histogram_data(
        displacements, bins['displacement'])
    distributions['force'] = get_histogram_data(forces, bins['force'])
    return distributions


# read atoms
temperature = 800
reference_structure = read('reference_structure.xyz')
ref_pos = reference_structure.get_positions()
rattle_structures = read('structures_rattle.extxyz@:')
mc_rattle_structures = read('structures_mc_rattle.extxyz@:')
md_structures = read('md_trajectory_T{}.traj@:'.format(temperature))

calc = EMT()

# generate distributions
rattle_distributions = get_distributions(rattle_structures, ref_pos, calc)
mc_rattle_distributions = get_distributions(
    mc_rattle_structures, ref_pos, calc)
md_distributions = get_distributions(md_structures, ref_pos, calc)

# plot
fs = 14
lw = 2.0
fig = plt.figure(figsize=(12, 5))
ax1 = fig.add_subplot(1, 3, 1)
ax2 = fig.add_subplot(1, 3, 2)
ax3 = fig.add_subplot(1, 3, 3)

units = OrderedDict(displacement='A', distance='A', force='eV/A')
for ax, key in zip([ax1, ax2, ax3], units.keys()):
    ax.plot(*rattle_distributions[key], label='rattle', lw=lw)
    ax.plot(*mc_rattle_distributions[key], label='Monte Carlo rattle', lw=lw)
    ax.plot(*md_distributions[key], label='MD {}K'.format(temperature), lw=lw)

    ax.set_xlabel('{} ({})'.format(key.title(), units[key]), fontsize=fs)
    ax.set_xlim([np.min(bins[key]), np.max(bins[key])])
    ax.set_ylim(bottom=0.0)
    ax.tick_params(labelsize=fs)
    ax.legend(fontsize=fs)

ax1.set_ylabel('Distribution', fontsize=fs)
plt.tight_layout()
plt.savefig('structure_generation_distributions.pdf')
