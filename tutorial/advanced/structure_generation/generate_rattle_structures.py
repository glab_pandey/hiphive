"""
Generate rattled structures using both the standard rattle approach and the
Monte Carlo rattle approach.

The parameters in this example are chosen to generate a similar
magnitude of the displacements using both methods.

This script may take a few minutes to run.
"""

from ase.build import bulk
from ase.io import write
from hiphive.structure_generation import (generate_rattled_structures,
                                          generate_mc_rattled_structures)


# parameters
alat = 3.52         # lattice parameter
size = 6            # supercell size
n_configs = 25      # number of configurations to generate
rattle_std = 0.12
min_distance = 0.67 * alat

# reference structure
prim = bulk('Ni', a=alat)

supercell = prim.repeat(size)
reference_positions = supercell.get_positions()
write('reference_structure.xyz', supercell)

# standard rattle
print('standard rattle')
structures_rattle = generate_rattled_structures(
    supercell, n_configs, rattle_std)
write('structures_rattle.extxyz', structures_rattle)

# Monte Carlo rattle
print('Monte Carlo rattle')
structures_mc_rattle = generate_mc_rattled_structures(
    supercell, n_configs, 0.25*rattle_std, min_distance, N_iter=20)
write('structures_mc_rattle.extxyz', structures_mc_rattle)
