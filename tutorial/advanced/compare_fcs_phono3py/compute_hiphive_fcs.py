from ase.io import read
from hiphive import StructureContainer, ForceConstantPotential
from hiphive.fitting import Optimizer
from hiphive.io.phonopy import write_phonopy_fc2, write_phonopy_fc3

supercell = read('phono3py_calculation/SPOSCAR')
# Read structure containers and cluster spaces
sc2 = StructureContainer.read('structure_container2')
sc3 = StructureContainer.read('structure_container3')
sc4 = StructureContainer.read('structure_container4')
cs2 = sc2.get_cluster_space()
cs3 = sc3.get_cluster_space()
cs4 = sc4.get_cluster_space()

# Fit models
opt = Optimizer(sc2.get_fit_data())
opt.train()
print(opt)
fcp2 = ForceConstantPotential(cs2, opt.parameters)

opt = Optimizer(sc3.get_fit_data())
opt.train()
print(opt)
fcp3 = ForceConstantPotential(cs3, opt.parameters)

opt = Optimizer(sc4.get_fit_data())
opt.train()
print(opt)
fcp4 = ForceConstantPotential(cs4, opt.parameters)

# Write force constants
# this would look smoother if we had our own hdf5 format for the full fcs!
fcs2 = fcp2.get_force_constants(supercell)
write_phonopy_fc2('fc2_model2.hdf5', fcs2)

fcs3 = fcp3.get_force_constants(supercell)
write_phonopy_fc2('fc2_model3.hdf5', fcs3)
write_phonopy_fc3('fc3_model3.hdf5', fcs3)

fcs4 = fcp4.get_force_constants(supercell)
write_phonopy_fc2('fc2_model4.hdf5', fcs4)
write_phonopy_fc3('fc3_model4.hdf5', fcs4)
