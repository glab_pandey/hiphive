import numpy as np
from ase.io import read
from hiphive.io.phonopy import read_phonopy_fc2, read_phonopy_fc3
from hiphive.core.utilities import compute_gamma_frequencies


def compute_relative_error(fcs1, fcs2):
    """ Return relative fc difference in percentage """
    return 100.0 * np.linalg.norm(fcs1 - fcs2) / np.linalg.norm(fcs2)


# Read phonopy fcs
atoms = read('phono3py_calculation/SPOSCAR')
fc2_phono3py = read_phonopy_fc2('phono3py_calculation/fc2.hdf5')
fc3_phono3py = read_phonopy_fc3('phono3py_calculation/fc3.hdf5')
omega_phonopy = compute_gamma_frequencies(fc2_phono3py, atoms.get_masses())

# Read hiphive fcs
fc2_model2 = read_phonopy_fc2('fc2_model2.hdf5')
omega_model2 = compute_gamma_frequencies(fc2_model2, atoms.get_masses())

fc2_model3 = read_phonopy_fc2('fc2_model3.hdf5')
fc3_model3 = read_phonopy_fc3('fc3_model3.hdf5')
omega_model3 = compute_gamma_frequencies(fc2_model3, atoms.get_masses())


fc2_model4 = read_phonopy_fc2('fc2_model4.hdf5')
fc3_model4 = read_phonopy_fc3('fc3_model4.hdf5')
omega_model4 = compute_gamma_frequencies(fc2_model4, atoms.get_masses())

# Compare fcs
fc2_model2_error = compute_relative_error(fc2_model2, fc2_phono3py)
omega_model2_error = compute_relative_error(omega_phonopy, omega_model2)
print('Model 2: FC2 error        {:.4f} %'.format(fc2_model2_error))
print('Model 2: Frequency error  {:.4f} %'.format(omega_model2_error))

print('')

fc2_model3_error = compute_relative_error(fc2_model3, fc2_phono3py)
fc3_model3_error = compute_relative_error(fc3_model3, fc3_phono3py)
omega_model3_error = compute_relative_error(omega_phonopy, omega_model3)
print('Model 3: FC2 error        {:.4f} %'.format(fc2_model3_error))
print('Model 3: FC3 error        {:.4f} %'.format(fc3_model3_error))
print('Model 3: Frequency error  {:.4f} %'.format(omega_model3_error))

print('')

fc2_model4_error = compute_relative_error(fc2_model4, fc2_phono3py)
fc3_model4_error = compute_relative_error(fc3_model4, fc3_phono3py)
omega_model4_error = compute_relative_error(omega_phonopy, omega_model4)
print('Model 4: FC2 error        {:.4f} %'.format(fc2_model4_error))
print('Model 4: FC3 error        {:.4f} %'.format(fc3_model4_error))
print('Model 4: Frequency error  {:.4f} %'.format(omega_model4_error))
