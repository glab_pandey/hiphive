"""
Tests shengBTE write and read third order force constants
"""

import numpy as np
import os
import tempfile

from ase.build import bulk
from itertools import product

from hiphive.io.shengBTE import read_shengBTE_fc3, write_shengBTE_fc3


tol = 1e-8

prim = bulk('Al', a=4.05, cubic=True)
supercell = prim.repeat(2)

Natoms = len(supercell)

# setup index permutation symmetrized fc3 array
fc3_dict_ref = {}
for triplet in product(*[range(Natoms)]*3):
    if triplet == tuple(sorted(triplet)):
        fc3_dict_ref[triplet] = np.random.random((3, 3, 3))

with tempfile.TemporaryDirectory() as test_dir:
    # Write and read fc3 to hdf5 file
    fc3_filename = os.path.join(test_dir, 'FORCE_CONSTANT_3RD')
    write_shengBTE_fc3(fc3_filename, prim, supercell, fc3_dict_ref)
    fc3_dict_read = read_shengBTE_fc3(fc3_filename, prim, supercell)

    # Note fc3_dict_read does not contain all original clusters/fcs in fc_dict
    for triplet, fc3_read in fc3_dict_read.items():
        assert triplet in fc3_dict_ref.keys(), 'Bad triplet found'+str(triplet)
        assert np.linalg.norm(fc3_read - fc3_dict_ref[triplet]) < tol, \
            'Large diff between written and read fc3'
