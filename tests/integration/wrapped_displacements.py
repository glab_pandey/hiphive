"""
Test get_wrapped_displacements
"""

import numpy as np

from ase.build import bulk
from hiphive.utilities import get_wrapped_displacements


atoms = bulk('Al').repeat(4)
ideal_positions = atoms.get_positions()

atoms.rattle(1.0)  # This causes atoms to go outside cell
ref_disps = atoms.positions - ideal_positions

atoms.wrap()
disps = get_wrapped_displacements(atoms.positions, ideal_positions, atoms.cell)

assert np.linalg.norm(ref_disps-disps) < 1e-8
