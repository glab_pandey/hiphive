import numpy as np
import sys
import tempfile
import unittest

from ase.build import bulk
from ase.calculators.singlepoint import SinglePointCalculator
from ase.calculators.emt import EMT
from hiphive import ClusterSpace, StructureContainer
from hiphive.structure_container import FitStructure, atoms_equals
from io import StringIO


class TestStructureContainer(unittest.TestCase):
    """
    Unittest class for StructureContainer.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # setup ClusterSpace
        cutoffs = [5.0]
        prim = bulk('Al', 'fcc', a=4.05)
        self.cs = ClusterSpace(prim, cutoffs)

        # setup supercells
        supercell = prim.repeat(4)
        calc = EMT()
        rattled_structures = []
        for i in range(5):
            atoms = supercell.copy()
            atoms.set_calculator(calc)
            atoms.rattle(0.05, seed=i)
            forces = atoms.get_forces()
            displacements = atoms.positions - supercell.positions
            atoms.positions = supercell.get_positions()
            atoms.calc = None
            atoms.new_array('displacements', displacements)
            atoms.new_array('forces', forces)
            rattled_structures.append(atoms)
        self.rattled_structures = rattled_structures

    def setUp(self):
        """ Create an empty StructureContainer. """
        self.sc = StructureContainer(self.cs)

    def test_init(self):
        """
        Test initializing StructureContainer.
        """
        # empty
        sc = StructureContainer(self.cs)
        self.assertIsInstance(sc, StructureContainer)

        # with FitStructures
        fs_list = [FitStructure(atoms) for atoms in self.rattled_structures]
        sc = StructureContainer(self.cs, fs_list)
        self.assertIsInstance(sc, StructureContainer)

    def test_len(self):
        """
        Test dunder len.
        """
        for structure in self.rattled_structures:
            self.sc.add_structure(structure, compute_fit_matrix=False)
            self.assertEqual(len(self.sc), len(self.sc._structure_list))

    def test_getitem(self):
        """
        Test dunder getitem.
        """
        for i, structure in enumerate(self.rattled_structures):
            self.sc.add_structure(structure, compute_fit_matrix=False)
        for i, structure in enumerate(self.rattled_structures):
            fs = self.sc[i]
            self.assertEqual(fs.atoms, structure)

    def test_data_shape(self):
        """
        Test data_shape property.
        """
        structure = self.rattled_structures[0]
        self.assertIsNone(self.sc.data_shape)
        self.sc.add_structure(structure, compute_fit_matrix=True)
        target_shape = (3 * len(structure), self.cs.number_of_dofs)
        self.assertEqual(self.sc.data_shape, target_shape)

    def test_cluster_space(self):
        """
        Test cluster_space property.
        """
        self.assertEqual(str(self.sc.cluster_space), str(self.cs))

    def test_get_structure_indices(self):
        """
        Test get structure indices via user tag.
        """
        tags = ['group-1', 'group-2']
        groups = [[0, 1, 2], [3, 4]]
        for group, tag in zip(groups, tags):
            for i in group:
                structure = self.rattled_structures[i]
                self.sc.add_structure(structure, user_tag=tag,
                                      compute_fit_matrix=False)

        for target_group, tag in zip(groups, tags):
            group = self.sc.get_structure_indices(tag)
            self.assertEqual(target_group, group)

    def test_write_and_read(self):
        """
        Test the write and read functionality.
        """

        # save
        temp_file = tempfile.TemporaryFile()
        for i, structure in enumerate(self.rattled_structures):
            self.sc.add_structure(structure, user_tag='tag-{}'.format(i),
                                  compute_fit_matrix=True)
        self.sc.write(temp_file)

        # read with fit matrices
        temp_file.seek(0)
        sc_read = StructureContainer.read(temp_file)

        # check
        self.assertEqual(str(sc_read), str(self.sc))
        M, f = self.sc.get_fit_data()
        M_read, f_read = sc_read.get_fit_data()
        np.testing.assert_almost_equal(M, M_read)
        np.testing.assert_almost_equal(f, f_read)

        # read without fit matrices
        temp_file.seek(0)
        sc_read = StructureContainer.read(temp_file, read_fit_matrices=False)
        temp_file.close()
        self.assertEqual(str(self.sc._cs), str(sc_read._cs))
        self.assertIsNone(sc_read.data_shape)

    def test_add_structure(self):
        """
        Test add_structure functionality.
        """

        # ignore if no displacements
        structure = self.rattled_structures[0].copy()
        structure.set_array('displacements', None)
        self.sc.add_structure(structure)
        self.assertEqual(len(self.sc._structure_list), 0)

        # ignore if no forces
        structure = self.rattled_structures[0].copy()
        structure.set_array('forces', None)
        self.sc.add_structure(structure)
        self.assertEqual(len(self.sc._structure_list), 0)

        # Adding with SinglePointCalculator
        structure = self.rattled_structures[0].copy()
        forces = structure.get_array('forces')
        structure.set_array('forces', None)  # delete forces array
        calc = SinglePointCalculator(structure, forces=forces)
        structure.set_calculator(calc)
        self.sc.add_structure(structure)
        self.assertEqual(len(self.sc._structure_list), 1)

        # ignore duplicates
        structure = self.rattled_structures[0].copy()
        self.sc.add_structure(structure)
        self.assertEqual(len(self.sc._structure_list), 1)

    def test_update_fit_data(self):
        """
        Test update fit data.
        """

        # Add structure without fit matrix
        structure = self.rattled_structures[0].copy()
        self.sc.add_structure(structure, compute_fit_matrix=False)

        # No fit data available
        self.assertIsNone(self.sc._structure_list[0].fit_matrix)

        # update
        self.sc.update_fit_data()
        self.assertIsNotNone(self.sc._structure_list[0].fit_matrix)

    def test_get_fit_data(self):
        """
        Test get fit data.
        """

        # get fit data from empty StructureContainer
        self.assertIsNone(self.sc.get_fit_data())

        # add structures
        for structure in self.rattled_structures:
            self.sc.add_structure(structure)

        # get partial fit data
        partial_indices = [0, 3]
        target_n_rows = 3 * len(structure) * len(partial_indices)
        M_partial, f_partial = self.sc.get_fit_data(partial_indices)
        self.assertEqual(M_partial.shape[0], target_n_rows)
        self.assertEqual(f_partial.shape[0], target_n_rows)

        # get all fit data (default behaviour)
        target_n_rows = 3 * len(structure) * len(self.rattled_structures)
        M_full, f_full = self.sc.get_fit_data()
        self.assertEqual(M_full.shape[0], target_n_rows)
        self.assertEqual(f_full.shape[0], target_n_rows)

    def test_str(self):
        """
        Test dunder str.
        """
        self.assertIsInstance(str(self.sc), str)

    def test_repr(self):
        """
        Test dunder repr.
        """
        self.assertIsInstance(repr(self.sc), str)

    def test_print_cluster_space(self):
        """
        Test print_cluster_space
        """
        with StringIO() as capturedOutput:
            sys.stdout = capturedOutput
            self.sc.print_cluster_space()
            sys.stdout = sys.__stdout__
            self.assertIn('Cluster Space', capturedOutput.getvalue())
            self.assertIn('Total number of orbits     : 4',
                          capturedOutput.getvalue())
            self.assertIn('Total number of clusters   : 22',
                          capturedOutput.getvalue())

    def test_atoms_equals(self):
        """
        Test the helper function atoms_equals
        """

        # set up atoms objects
        atoms1 = bulk('Al', 'fcc', a=4.05).repeat((3, 2, 1))
        atoms1.new_array('forces', np.random.random((len(atoms1), 3)))
        atoms2 = atoms1.copy()

        # check that they are equal
        self.assertTrue(atoms_equals(atoms1, atoms2))

        # False when pbc is changed
        atoms2_copy = atoms2.copy()
        atoms2_copy.pbc = [False, True, True]
        self.assertFalse(atoms_equals(atoms1, atoms2_copy))

        # False when cell is changed
        atoms2_copy = atoms2.copy()
        atoms2_copy.cell[1][2] += 0.1
        self.assertFalse(atoms_equals(atoms1, atoms2_copy))

        # False when different number of arrays
        atoms2_copy = atoms2.copy()
        atoms2_copy.new_array('arr', np.random.random((len(atoms2_copy), 3)))
        self.assertFalse(atoms_equals(atoms1, atoms2_copy))

        # False when different named arrays
        atoms2_copy = atoms2.copy()
        atoms2_copy.set_array('forces', None)  # delete array
        atoms2_copy.new_array('arr', np.random.random((len(atoms2_copy), 3)))
        self.assertFalse(atoms_equals(atoms1, atoms2_copy))


class TestFitStructure(unittest.TestCase):
    """
    Unittest class for FitStructure.

    FitStructure will implicitly be tested from the StructureContainer tests.
    """

    def setUp(self):
        self.atoms = bulk('Al', 'fcc', a=4.05).repeat((3, 2, 1))
        self.fs = FitStructure(self.atoms, user_tag='mytest')

    def test_str(self):
        """
        Test dunder str.
        """
        self.assertIsInstance(str(self.fs), str)

    def test_repr(self):
        """
        Test dunder repr.
        """
        self.assertIsInstance(repr(self.fs), str)


if __name__ == '__main__':
    unittest.main()
