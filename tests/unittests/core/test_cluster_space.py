import unittest
import tempfile
import numpy as np
import sys

from ase.build import bulk
from hiphive import ClusterSpace
from hiphive.cluster_filter import Cutoffs
from io import StringIO


class TestClusterSpace(unittest.TestCase):
    """
    Unittest class for ClusterSpace.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.prim = bulk('Al', 'fcc', a=4.05)
        self.cutoffs = [5.0]

    def test_init_with_nonpbc(self):
        """
        Tests that initializing ClusterSpace with non pbc structure fails.
        """

        # run with pbc
        ClusterSpace(self.prim, self.cutoffs)

        # all pbc false
        with self.assertRaises(ValueError):
            prim_tmp = self.prim.copy()
            prim_tmp.pbc = False
            ClusterSpace(prim_tmp, self.cutoffs)

        # pbc false in one direction
        with self.assertRaises(ValueError):
            prim_tmp = self.prim.copy()
            prim_tmp.pbc = [1, 0, 1]
            ClusterSpace(prim_tmp, self.cutoffs)

    def test_init_with_cutoffs(self):
        """
        Test that initializing ClusterSpace with Cutoffs object works
        """
        cutoffs_obj = Cutoffs(np.array([[5.0, 5.0, 5.0], [4.0, 4.0, 4.0]]))
        ClusterSpace(self.prim, cutoffs_obj)

    def test_print_orbits(self):
        """
        Test that print orbits works
        """
        cs = ClusterSpace(self.prim, self.cutoffs)
        with StringIO() as capturedOutput:
            sys.stdout = capturedOutput
            cs.print_orbits()
            sys.stdout = sys.__stdout__
            self.assertIn('List of Orbits', capturedOutput.getvalue())
            self.assertIn('Al Al        |  2.4801  |       (0, 2)',
                          capturedOutput.getvalue())

    def test_write_and_read(self):
        """
        Test the write and read functionality.
        """
        cs = ClusterSpace(self.prim, self.cutoffs)

        # test with file object
        with tempfile.TemporaryFile() as file:
            cs.write(file)
            file.seek(0)
            cs_read = ClusterSpace.read(file)
            self.assertEqual(str(cs), str(cs_read))

        # test with file name
        with tempfile.NamedTemporaryFile() as file:
            cs.write(file.name)
            cs_read = ClusterSpace.read(file.name)
            self.assertEqual(str(cs), str(cs_read))


if __name__ == '__main__':
    unittest.main()
