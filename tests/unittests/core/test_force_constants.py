import unittest
import tempfile
import numpy as np

from ase.build import bulk
from hiphive import ForceConstants
from hiphive.force_constants import compute_gamma_frequencies,\
                                    symbolize_force_constant, fc_array_to_dict


class TestForceConstants(unittest.TestCase):
    """
    Unittest class for ForceConstants.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # setup dummy cluster_groups and fc_list and fc_dict
        cluster_groups = [[(0, 0), (1, 1), (2, 2), (3, 3)],
                          [(0, 1), (0, 2), (1, 2), (1, 3), (2, 3)],
                          [(0, 1, 1), (1, 2, 2), (2, 3, 3)]]
        fc_list = [np.random.random((3, 3)),
                   np.random.random((3, 3)),
                   np.random.random((3, 3, 3))]
        fc_dict = {}
        for c_list, fc in zip(cluster_groups, fc_list):
            for c in c_list:
                fc_dict[c] = fc

        self.orders = list(set([len(c_list[0]) for c_list in cluster_groups]))
        self.cluster_groups = cluster_groups
        self.fc_list = fc_list
        self.fc_dict = fc_dict
        self.atoms = bulk('Al', 'fcc', a=4.0).repeat((2, 2, 1))
        self.natoms = len(self.atoms)

    def setUp(self):
        """ Create an new ForceConstants from cluster_groups and fc_list. """
        self.fcs = ForceConstants(cluster_groups=self.cluster_groups,
                                  fc_list=self.fc_list,
                                  atoms=self.atoms)

    def test_init(self):
        """
        Test initializing ForceConstants.
        """

        # initialize via cluster_groups and fc_list
        fcs = ForceConstants(cluster_groups=self.cluster_groups,
                             fc_list=self.fc_list, atoms=self.atoms)
        self.assertIsInstance(fcs, ForceConstants)
        self.assertEqual(fcs.orders, self.orders)

        # initialize via fc_dict
        fcs = ForceConstants(fc_dict=self.fc_dict, atoms=self.atoms)
        self.assertIsInstance(fcs, ForceConstants)
        self.assertEqual(fcs.orders, self.orders)

    def test_setup_atoms(self):
        """
        Test setup_atoms
        """

        # no atoms object
        self.fcs._setup_atoms(None)
        self.assertSequenceEqual(set(range(self.natoms)),
                                 self.fcs._allowed_atoms)
        self.assertIsNone(self.fcs._atoms)

        # with atoms object1
        self.fcs._setup_atoms(self.atoms)
        self.assertSequenceEqual(set(range(self.natoms)),
                                 self.fcs._allowed_atoms)
        self.assertEqual(self.fcs._atoms, self.atoms)

    def test_setup_fc_dict(self):
        """
        TODO: Unclear how this should work
        """
        pass

    def test_get_fc_array(self):
        """
        Test get_fc_array
        """

        # assert ValueError for unavaible orders
        not_available = set(range(10)) - set(self.orders)
        for order in not_available:
            with self.assertRaises(ValueError):
                self.fcs.get_fc_array(order=order)

        # check shape is ok for available orders
        for order in self.orders:
            fc_array = self.fcs.get_fc_array(order=order)
            target_shape = (self.natoms,) * order + (3,) * order
            self.assertSequenceEqual(fc_array.shape, target_shape)

    def test_get_fc_dict(self):
        """
        TODO
        """
        pass

    def test_getitem(self):
        """
        TODO
        """
        pass

    def test_len(self):
        """
        Test dunder len
        """
        self.assertEqual(len(self.fcs._fc_dict), len(self.fcs))

    def test_write_and_read(self):
        """
        Test the write and read functionality.
        """

        # save
        temp_file = tempfile.TemporaryFile()
        self.fcs.write(temp_file)

        # read
        temp_file.seek(0)
        fcs_read = ForceConstants.read(temp_file)

        # check
        self.assertEqual(str(self.fcs), str(fcs_read))
        self.assertSequenceEqual(self.fcs.clusters, fcs_read.clusters)
        for c in self.fcs.clusters:
            np.testing.assert_almost_equal(self.fcs[c], fcs_read[c])

    def test_assert_acoustic_sum_rules(self):
        """
        Test assert_acoustic_sum_rules.
        """

        # make fc2 for which all sum rules are enforced
        fc2_array = np.zeros((10, 10, 3, 3))
        for i in range(10):
            for j in range(i, 10):
                fc2_ij = np.random.random((3, 3))
                fc2_array[i, j] = fc2_ij
                fc2_array[j, i] = fc2_ij.T
            fc2_array[i, i] = np.zeros((3, 3))
            fc2_array[i, i] = - np.sum(fc2_array[i], axis=0)

        # check that assert sum rules passes
        fc2_dict = fc_array_to_dict(fc2_array)
        fcs = ForceConstants(fc2_dict)
        fcs.assert_acoustic_sum_rules()

        # make fc2 for which all sum rules are enforced
        fc2_dict[(0, 0)] += 0.2
        fcs = ForceConstants(fc2_dict)
        # check that assert rum rules raises AssertionError
        with self.assertRaises(AssertionError):
            fcs.assert_acoustic_sum_rules()

    def test_compute_gamma_frequencies(self):
        """
        Test compute_gamma_frequencies function.
        """
        freqs = compute_gamma_frequencies(self.fcs, self.atoms.get_masses())
        self.assertEqual(len(freqs), 3 * self.natoms)

    def test_symbolize_force_constants(self):
        """
        Test symbolize_force_constant function.
        """

        # test with diagonal second order fc
        fc2 = np.array([[2.2, 0, 0], [0, 2.2, 0], [0, 0, 2.2]])
        fc2_sym = symbolize_force_constant(fc2)
        for i in range(3):
            # diagonal elements
            self.assertEqual('a', fc2_sym[i][i])
            for j in set(range(3))-set([i]):
                # off diagonal elements
                self.assertEqual(0, fc2_sym[i][j])

        # test with negative value
        fc2 = np.array([[1, 2, 3], [0, 0, 0], [-1, -2, -3]])
        fc2_sym = symbolize_force_constant(fc2)
        for i in range(3):
                self.assertEqual('-' + fc2_sym[0][i], fc2_sym[2][i])
                self.assertEqual(0, fc2_sym[1][i])


if __name__ == '__main__':
    unittest.main()
