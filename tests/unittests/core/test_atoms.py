import unittest

from hiphive.core.atoms import Atom


class AtomTest(unittest.TestCase):

    def setUp(self):
        self.orginal_list = (5, 2, 3, 4)
        self.atom = Atom(self.orginal_list)

    def test_site(self):
        self.assertEqual(self.orginal_list[0], self.atom.site)

    def test_offset(self):
        self.assertEqual(self.orginal_list[1:], self.atom.offset)


if __name__ == '__main__':
    unittest.main()
