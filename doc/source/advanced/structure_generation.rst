.. _advanced_structure_generation:
.. highlight:: python
.. index::
   single: Advanced topics; Structure generation

Structure generation
====================

The construction of force constant potentials (FCPs) is crucially dependent on
the availability of suitable training data. Once configurations are available
the atomic forces can be readily computed, typically using methods such as
density functional theory (DFT).

It is, however, essential that the configurations in the training and
validation sets represent a distribution of displacements (and ultimately
forces) that resemble configurations that are observed under dynamic conditions.
While it is of course possible to run (*ab-initio*) molecular dynamics (MD)
simulations to generate such configurations, it is computationally desirable to
limit the number of such calculations.

It is straightforward to generate "rattled" configurations by randomly drawing
displacements from a normal distribution. This is achieved e.g., by the
:func:`rattle` function of the ASE :class:`Atoms` class. In the present example
it is shown that while this approach yields displacement distributions that
resemble MD simulations the distribution of forces exhibits a long tail with
some atoms experiencing extremely large forces. This behavior can be traced to
some interatomic distances becoming very small whence the atoms sample the very
steep repulsive branch of the interaction potential.

To circumvent this problem :program:`hiPhive` implements a modified rattle
procedure, which combines randomly drawing displacements with a Monte Carlo
trial step that penalizes displacements that lead to very small interatomic
distances. The resulting displacement and force distributions mimic the results
from MD simulations and thus provide sensible input configurations for
reference calculations without the need to carry out MD simulations using the
reference method.

.. figure:: _static/structure_generation_distributions.svg

  Distribution of displacements, distances, and forces for structures
  obtained by the standard rattle and Monte Carlo rattle procedures as
  well as MD simulations.

Source code
-----------

.. |br| raw:: html

   <br />

.. container:: toggle

    .. container:: header

       Rattled structures are generated in |br|
       ``tutorial/advanced/structure_generation/generate_rattle_structures.py``

    .. literalinclude:: ../../../tutorial/advanced/structure_generation/generate_rattle_structures.py

.. container:: toggle

    .. container:: header

       Structures from molecular dynamics (MD) simulations are generated in |br|
       ``tutorial/advanced/structure_generation/generate_md_structures.py``

    .. literalinclude:: ../../../tutorial/advanced/structure_generation/generate_rattle_structures.py

.. container:: toggle

    .. container:: header

       The force distributions are analyzed in |br|
       ``tutorial/advanced/structure_generation/analyze_force_distribution.py``

    .. literalinclude:: ../../../tutorial/advanced/structure_generation/analyze_force_distribution.py
