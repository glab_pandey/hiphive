.. _advanced_learning_curve:
.. highlight:: python
.. index::
   single: Advanced topics; Learning curve

Learning curve
==============

This tutorial demonstrates the convergence behavior of two common optimization
algorithms with respect to training set size. Specifically, it considers
standard `least-squares regression
<https://en.wikipedia.org/wiki/Least_squares>`_ and `automated relevance
detection regression (ARDR) <http://scikit-
learn.org/stable/auto_examples/linear_model/plot_ard.html>`_.

Following the definition of basic parameters and the generation of a set of
reference structures (10), a fourth-order model for EMT Ni is prepared. Based
on these data fit matrix and target vector are constructed. Subsequently, the
`CrossValidationEstimator <fitting.CrossValidationEstimator>`_ is used to
obtain the evoluation of the cross validation (CV) score as a function of
training set size and the results are plotted.

.. warning::

  Please note that calling functions that rely on the generation of pseudo-
  random numbers *repeatedly with the same seed* (i.e., repeatedly falling back
  to the default value) is **strongly** discouraged as it will lead to
  correlation. To circumvent this problem one can for example seed a sequence
  of random numbers and then use these numbers in turn as seeds.

.. figure::
    _static/learning_curve.svg

Source code
-----------

.. |br| raw:: html

   <br />

.. container:: toggle

    .. container:: header

        The complete source code is available in |br|
        ``tutorial/advanced/learning_curve/learning_curve.py``

    .. literalinclude:: ../../../tutorial/advanced/learning_curve/learning_curve.py
