.. _tutorial_advanced_topics:
.. index:: Tutorial, advanced topics

Advanced topics
***************

The following tutorial sections illustrate some more advanced applications of
:program:`hiPhive`.

Some of the following example employ `phonopy
<https://atztogo.github.io/phonopy/>`_ and `phono3py
<https://atztogo.github.io/phono3py/>`_ for analyzing force constants from
:program:`hiPhive`. Please consult the respective websites for installation
instructions.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   cluster_analysis
   structure_generation
   effective_harmonic_models
   compare_fcs
   learning_curve
   feature_selection
   anharmonic_energy_surface
   interface_with_sklearn