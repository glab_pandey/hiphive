.. _advanced_compare_fcs:
.. highlight:: python
.. index::
   single: Advanced topics; Compare force constants

Compare force constants
=======================

This tutorial demonstrates that :program:`hiPhive` reproduces second and third
order force constants obtained using `phonopy
<https://atztogo.github.io/phonopy/>`_ and/or `phono3py
<https://atztogo.github.io/phono3py/>`_. Please consult the respective websites
for installation instructions for these codes. The present example furthermore
employs the `lammps <http://lammps.sandia.gov/>`_ calculator from `ASE
<https://wiki.fysik.dtu.dk/ase/index.html>`_, which requires a working `lammps
<http://lammps.sandia.gov/>`_ installation.

Preparation of reference data
-----------------------------

First we generate reference force constant matrices using :program:`phono3py`

.. container:: toggle

    .. container:: header

       ``tutorial/advanced/compare_fcs_phono3py/compute_phono3py_fcs.py``

    .. literalinclude:: ../../../tutorial/advanced/compare_fcs_phono3py/compute_phono3py_fcs.py


Training of force constant potentials
-------------------------------------

In order to obtain force constant matrices from :program:`hiPhive` we then
generate input structures with random displacements and compute the computing
forces.

.. container:: toggle

    .. container:: header

       ``tutorial/advanced/compare_fcs_phono3py/prepare_training_structures.py``

    .. literalinclude:: ../../../tutorial/advanced/compare_fcs_phono3py/prepare_training_structures.py

Next these input structures are compiled into a structure container.

.. container:: toggle

    .. container:: header

       ``tutorial/advanced/compare_fcs_phono3py/setup_containers.py``

    .. literalinclude:: ../../../tutorial/advanced/compare_fcs_phono3py/setup_containers.py

Afterwards force constant potentials are trained and used to set up force
constant matrices.

.. container:: toggle

    .. container:: header

       ``tutorial/advanced/compare_fcs_phono3py/compute_hiphive_fcs.py``

    .. literalinclude:: ../../../tutorial/advanced/compare_fcs_phono3py/compute_hiphive_fcs.py


Comparison of force constant matrices
-------------------------------------

Finally, the force constant matrices from :program:`hiPhive` and :program:`phono3py` are compared using the Froebenius norm.

.. math::

    \Delta = ||\matrix{\Phi}_\text{hiPhive} - \matrix{\Phi}_\text{phono3py}||_2 / ||\matrix{\Phi}_\text{phono3py}||_2

We also compute the relative error for the (easily computed), :math:`\Gamma` frequencies obtained using the different second order force constants.


.. container:: toggle

    .. container:: header

       ``tutorial/advanced/compare_fcs_phono3py/compare_fcs.py``

    .. literalinclude:: ../../../tutorial/advanced/compare_fcs_phono3py/compare_fcs.py

The results are compiled in the following table, which illustrates that it is
crucial to include higher-order terms in the expansion in order to recover the
(lower-order) terms of interest.

+-------------+---------------+---------------+---------------------+
| Terms       | FC2 error (%) | FC3 error (%) | Frequency error (%) |
+-------------+---------------+---------------+---------------------+
| 2nd         | 1.6209        |               | 1.5564              |
+-------------+---------------+---------------+---------------------+
| 2nd+3rd     | 0.8561        | 2.5278        | 0.4223              |
+-------------+---------------+---------------+---------------------+
| 2nd+3rd+4th | 0.1129        | 1.1812        | 0.0601              |
+-------------+---------------+---------------+---------------------+