.. _tutorial_basics:
.. index:: Tutorial, basics

Basic tutorial
**************

This tutorial serves as a hands-on introduction to :program:`hiPhive` and
provides an overview of its key features.

.. toctree::
   :maxdepth: 2
   :caption: Contents

   prepare_reference_data
   construct_fcp
   compute_harmonic_thermal_properties
   compute_third_order_properties
   run_fourth_order_MD
