.. index::
   single: Function reference; structures

.. module:: hiphive

Structures
==========

.. index::
   single: Class reference; StructureContainer

StructureContainer
------------------

.. autoclass:: StructureContainer
   :members:
   :undoc-members:


.. index::
   single: Class reference; FitStructure

FitStructure
------------

.. autoclass:: hiphive.structure_container.FitStructure
   :members:
   :undoc-members:


.. index::
   single: Structure generation

Structure generation
--------------------

.. automodule:: hiphive.structure_generation
   :members:
   :undoc-members:


.. index::
   single: Function reference; utilities

Utilities
---------

.. automodule:: hiphive.utilities
   :members:
   :undoc-members:
