.. index::
   single: Function reference; core components
   single: Class reference; core components

Core components
===============

The low-level functions and classes described here are part of the
:program:`hiPhive` core components and not intended to be used *directly*
during normal operation.

`cluster_space_builder`
-----------------------
.. automodule:: hiphive.core.cluster_space_builder
   :members:
   :undoc-members:

`constraints`
-------------
.. automodule:: hiphive.core.constraints
   :members:
   :undoc-members:

`get_clusters`
--------------
.. automodule:: hiphive.core.get_clusters
   :members:
   :undoc-members:

`get_orbits`
------------
.. automodule:: hiphive.core.get_orbits
   :members:
   :undoc-members:

`orbit`
-------
.. automodule:: hiphive.core.orbit
   :members:
   :undoc-members:

`orientation_family`
--------------------
.. automodule:: hiphive.core.orientation_family
   :members:
   :undoc-members:

`eigentensors`
--------------
.. automodule:: hiphive.core.eigentensors
   :members:
   :undoc-members:

`tensors`
---------
.. automodule:: hiphive.core.tensors
   :members:
   :undoc-members:

`atoms`
-------
.. automodule:: hiphive.core.atoms
   :members:
   :undoc-members:

`relate_structures`
-------------------
.. automodule:: hiphive.core.relate_structures
   :members:
   :undoc-members:


`utilities`
-----------
.. automodule:: hiphive.core.utilities
   :members:
   :undoc-members:

`core_config`
-------------
.. automodule:: hiphive.core.core_config
   :members:
   :undoc-members:
