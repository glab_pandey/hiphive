.. index:: Glossary

Glossary
********

General
=======
.. glossary::

   BCC
        Several metallic including for example elements from groups 5 (V, Nb,
        Ta) and 6 (Cr, Mo, W) have a `body-centered cubic (BCC)
        <https://en.wikipedia.org/wiki/Cubic_crystal_system>`_ ground state
        structure.

   FCC
        The `face-centered cubic (FCC) lattice
        <https://en.wikipedia.org/wiki/Cubic_crystal_system>`_ is one of the
        most common crystal structures for metallic elements including e.g.,
        the late transition metals from group 10 (Ni, Pd, Pt) and 11 (Cu, Ag,
        Au).

   DFT
        The construction of force constants requires accurate reference data.
        `Density functional theory (DFT)
        <https://en.wikipedia.org/wiki/Density_functional_theory>`_
        calculations are one of the most common source for such data.


Optimization and machine learning
=================================
.. glossary::

   Automatic relevance determination regression
        Automatic relevance determination regression (ARDR) is an optimization
        algorithm provided by `scikit-learn
        <http://scikit-learn.org/stable/auto_examples/linear_model/plot_ard.html>`_

   Compressive sensing
        `Compressive sensing (CS)
        <https://en.wikipedia.org/wiki/Compressed_sensing>`_, also known as
        compressive sampling, is an efficient method for constructing sparse
        solutions for linear systems.

   LASSO
        The `least absolute shrinkage and selection operator (LASSO)
        <https://en.wikipedia.org/wiki/Lasso_(statistics)>`_ is a method for
        performing variable selection and regularization in problems in
        statistics and machine learning.

   Kernel ridge regression
        `Kernel ridge regression (KRR) <http://scikit-
        learn.org/stable/modules/kernel_ridge.html>`_ combines `ridge
        regression <https://en.wikipedia.org/wiki/Tikhonov_regularization>`_
        with the `kernel trick <https://en.wikipedia.org/wiki/Kernel_method>`_.

   Regularization
        `Regularization
        <https://en.wikipedia.org/wiki/Regularization_(mathematics)>`_,
        is commonly used in machine learning to combat overfitting and
        for solving underdetermined systems.


Crystal symmetry and clusters
=============================
.. glossary::

   Crystal symmetry operation
        A crystal symmetry operation for a specific lattice means that the
        lattice is invariant under this operation. An operation comprises
        translational and rotational components.

   Cluster
        A cluster is defined as a set of points on a lattice.

   Cluster size
        The size of a cluster (commonly refered to as the cluster radius) is
        defined as the average distance to the geometrical center of the cluster.

   Cluster space
        The set of clusters into which a structure can be decomposed.

   Cutoff
        Cutoffs define the longest allowed distance between two atoms in a
        cluster for each order.

   Orbit
        An orbit is defined as a set of symmetry equivalent clusters

   Orientation family
        An orientation family is a subgroup to an orbit, which contains those
        clusters that are oriented identically in the lattice.


Force constants
===============
.. glossary::

   Irreducible parameters
        Many elements of the force constant matrices are related to each other
        by symmetry operations. The irreducible set up of parameters is
        obtained by applying all symmetry operations allowed by the space group
        of the ideal lattice and the sum rules.

   Sum rules
        In order for a force constant potential to fulfill translational
        invariance certain constraints are imposed on the force constants.
        These constraints are commonly referred to as sum rules.
