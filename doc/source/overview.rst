.. index:: Overview

Overview
********

Lattice vibrations
==================

The atoms in a material undergo regular vibrational motion around their
equilibrium positions, a phenomenon that is of fundamental importance for the
overall behavior of the material. In crystalline solids in particular these
vibrations are periodic in nature and can be described using quasi-particles
named phonons that represent *collective excitations* of the crystal lattice.

At the first level of approximation, phonons can be obtained within the so-called
harmonic approximation, which implies non-interacting quasi-particles
with (accordingly) infinite lifetimes. In principle, harmonic theory allows one
to obtain a wealth of information including phonon dispersions and Helmholtz
free energies. Using a slight extension, known as the quasi-harmonic
approximation, it is even possible to predict e.g., thermal expansion. The
approach has, however, various limitations related e.g., to the fact that real
materials do always exhibit some degree of anharmonicity, which is most
pronounced at elevated temperatures but even at low temperatures responsible
for e.g., temperature dependent frequencies, finite phonon life times, or a
finite thermal conductivity. In addition there are many materials, which are
mechanically unstable at zero temperature but stabilized by vibrations at
finite temperature (e.g., the group-IV transition metals in BCC structure,
cubic ZrO\ :sub:`2`\ , various cubic perovskite structures such as BaTiO\
:sub:`3`)\ . In all these cases one must explicitly account for the
anharmonicity of the lattice vibrations, which leads to phonon-phonon coupling.

Force constants
===============

The most essential ingredient required for analyzing phonons in a material is
the set of :ref:`force constants (FCs) <force_constants>`. Knowledge of the
latter allows one to compute the forces on the atoms solely based on their
displacements. Tools such as `phonopy <https://atztogo.github.io/phonopy/>`_
[TogTan15]_ exist for obtaining and analyzing the second order FCs, whereas
third order FCs can be extracted using e.g., `phono3py
<https://atztogo.github.io/phono3py/>`_ [TogChaTan15]_ or `shengBTE
<http://www.shengbte.org/>`_ [LiCarKat14]_. There are even codes such as
`alamode <http://alamode.readthedocs.io/en/latest/intro.html>`_ [TadGohTsu14]_
that enable one to obtain the fourth order FCs. While `phonopy
<https://atztogo.github.io/phonopy/>`_ and `shengBTE
<http://www.shengbte.org/>`_ employ finite displacement schemes to construct
FCs and typically carry out a systematic enumeration of the possible
displacements up to a certain cutoff, `alamode
<http://alamode.readthedocs.io/en/latest/intro.html>`_ relies on forces from
molecular dynamics (MD) simulations as input.

It is also possible to obtain FCs from density functional perturbation theory
(DFPT) calculations as implemented e.g., in the `abinit
<https://www.abinit.org/>`_ and `quantum-espresso <http://www.quantum-
espresso.org/>`_. In that case the FCs are constructed by transformation of the
dynamical matrices obtained on a regular **q**-point mesh.

The :program:`hiPhive` approach
===============================

:program:`hiPhive` enables one to efficiently obtain high order FCs (e.g., of
fourth or sixth order) including large and low-symmetry systems. It employs a
supercell approach similar to `phonopy <https://atztogo.github.io/phonopy/>`_,
`shengBTE <http://www.shengbte.org/>`_, or `alamode
<http://alamode.readthedocs.io/en/latest/intro.html>`_ but does not rely on a
specific type of input configuration (i.e. enumerated displacements or
configurations from MD simulations). Rather it employs advanced optimization
techniques that are designed to find sparse solutions, which in the present
case reflect the short-range nature of the FCs. If the input configurations are
constructed *sensibly* this approach allows one to obtain FCs using a much
smaller number of input configurations and thus to reduce the computational
effort, usually in the form of density functional theory (DFT) calculations,
considerably. This approach becomes genuinely advantageous already for
obtaining second order FCs in large and/or low symmetry systems (defects,
interfaces, surfaces, large unit cells etc). :program:`hiPhive` truly excels
when it comes to higher order FCs, for which a strict enumeration scheme
quickly leads to an explosion of displacement calculations.
