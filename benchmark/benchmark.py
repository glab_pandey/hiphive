#!/usr/bin/python3
"""
Runs all benchmarks
"""
import argparse
import time
from ase.io import read
from logging import Formatter, FileHandler
from hiphive.io.logging import logger
from hiphive import ClusterSpace
from hiphive.force_constant_model import ForceConstantModel


available_systems = ['Ti-6', 'clathrate-2', 'Ta-vacancy-2']

logger.setLevel(10)
logger.handlers[0].setLevel(20)


def setup_system(system):
    assert system in available_systems
    if system == 'Ti-6':
        atoms = read('structures/Ti_bcc_6x6x6_rattled.extxyz')
        cutoffs = [3.5, 3.5, 3.5, 3.0, 3.0]
    elif system == 'clathrate-2':
        atoms = read('structures/clathrate_BGG54_md600K.extxyz')
        cutoffs = [4.0]
    elif system == 'Ta-vacancy-2':
        atoms = read('structures/Ta_bcc_vacancy_rattled.extxyz')
        cutoffs = [6.0]

    return atoms, cutoffs


def benchmark_system(system, logfile, skip_fit_matrix=False):

    # setup system
    atoms, cutoffs = setup_system(system)

    # ClusterSpace
    logger.info('Starting building ClusterSpace')
    cs = ClusterSpace(atoms, cutoffs)
    logger.info('Finished building ClusterSpace\n\n')

    # ForceConstantModel
    logger.info('Starting building ForceConstantModel')
    fcm = ForceConstantModel(atoms, cs)
    logger.info('Finished building ForceConstantModel\n\n')

    # Fit Matrix
    if not skip_fit_matrix:
        displacements = atoms.get_array('displacements')
        logger.info('Starting computing fit matrix')
        fcm.get_fit_matrix(displacements)
        logger.info('Finished computing fit matrix')


# command line parser
parser = argparse.ArgumentParser(
    description='Run benchmarks systems and output verbose timestamped logs')
parser.add_argument('-s', '--systems', type=int, metavar='i',
                    default=[1, 2, 3], nargs='+',
                    help='Specify which systems to run as 1 2 3 for {}'.format(
                        available_systems))
parser.add_argument('--tag', default=None, type=str,
                    help='name of the benchmark (gets added to log filename)')
parser.add_argument('--skip-fit-matrix', default=False, action='store_true',
                    help='skip the calculation of the fit-matrix')

# Collect args
args = parser.parse_args()
skip_fit_matrix = args.skip_fit_matrix
run_systems = [available_systems[index-1] for index in args.systems]
if len(run_systems) == 0:
    raise ValueError('No valid systems in {}'.format(args.systems))
tag = args.tag
if tag is not None:
    tag = '_{}_'.format(tag)
else:
    tag = ''

# run benchmarks
for system in run_systems:

    # setup logger
    date_time = time.strftime("%Y-%m-%d-%H.%M")
    logfile = 'logs/benchmark_{}{}_{}'.format(system, tag, date_time)
    filehandler = FileHandler(logfile)
    logger.addHandler(filehandler)

    # formatter
    fmt = '%(asctime)s.%(msecs)03d      %(message)s'
    datefmt = '%Y-%m-%d %H:%M:%S'
    formatter = Formatter(fmt=fmt, datefmt=datefmt)
    logger.handlers[1].setFormatter(formatter)

    # benchmark
    benchmark_system(system, logfile, skip_fit_matrix=skip_fit_matrix)

    # remove logger filehandler so a new can be attached
    logger.removeHandler(filehandler)
